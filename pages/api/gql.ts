import { databaseRequest } from '../../lib/databaseConnection';
import { NextApiHandler} from "next";

const NextApiHandler: NextApiHandler = async(req, res) => {
  const isAuthorized = req?.headers.authorization === "Bearer some-token";
  console.log("isAuthorized", req?.headers.authorization);

  if (isAuthorized) {
   const { query, variables } = req.body;
  const result = await databaseRequest({ query, variables });
  return res.json(result);
  } else {
    return res.status(401).json({error: "Unauthorized"});
  }
};

export default NextApiHandler;
/*
export default async function handler(req, res) {
  const { query, variables } = req.body
  const result = await databaseRequest({ query, variables })
  return res.json(result)
}
*/
